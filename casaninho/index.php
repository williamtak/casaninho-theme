<?php
get_header();
?>

<body>


    <div class="container">
      <div class="row">
	<h1 class="text-center fw-bold my-5"><em>Blog</em></h1>
<div class="col-md-9">
  <div class="row justify-content-center">
  <?php query_posts('posts_per_page=20'); ?>
  <?php
// Start the Loop.

$the_query = new WP_Query( $args ); 

if ( have_posts() ) : 
    while ( have_posts() ) : the_post();
    
        /* * See if the current post is in category 3.
          * If it is, the div is given the CSS class "post-category-three".
          * Otherwise, the div is given the CSS class "post".
        */
        if ( in_category( 3 ) ) : ?>
        <div class="post-category-three">
        <?php else : ?>
        <div class="col-md-5 col-sm-12 my-2">
    <div class="border-frame-top"></div>
    <div class="row align-items-center py-3"  style="min-height: 200px;">
      <a class="mr-2" style="width: auto;" href="<?php the_permalink();?>"><?php the_post_thumbnail( array( 'class' => 'img-thumbnail')); ?></a>
        <div class="col-md-6">
          <h5 class="card-title"><em><?php the_title( ); ?></em></h5>
          <p class="card-text"><?php echo wp_trim_words( get_the_content( ), 12, '...' ); 
  ?></p>
        </div>
    </div>
    <div class="border-frame-bottom"></div>

  </div>
      <?php endif; ?>
      <?php 
    // Stop the Loop, but allow for a "if not posts" situation
    endwhile; 

else :
    /*
      * The very first "if" tested to see if there were any posts to
      * display. This "else" part tells what do if there weren't any.
     */
     _e( 'Sorry, no posts matched your criteria.', 'textdomain' );
  
// Completely stop the Loop.
 endif;?>
  </div>
  </div>

<!--Sidebar-->
<div class="col-md-3">
<?php get_sidebar(); ?>
</div>

</div>
</div>

<?php
get_footer();
