<?php


//setup inicial
function casaninho_theme_support() {
    add_theme_support( 'title-tag' );

//Logo
    add_theme_support('custom-logo');
    add_theme_support( 'post-thumbnails' );
    set_post_thumbnail_size(1280, 720, true);
}


/**
 * Register Custom Navigation Walker
 */
function register_navwalker(){
	require_once get_template_directory() . '/class-wp-bootstrap-navwalker.php';
}
add_action( 'after_setup_theme', 'register_navwalker' );



add_action( 'after_setup_theme', 'casaninho_theme_support' );

if (!function_exists('wp_render_title_tag'))    {
    function casaninho_render_title(){
        ?><title><?php wp_title('|', true, 'right');?> <?php bloginfo('name'); ?></title> <?php
    }
    add_action('wp_head', 'casaninho_render_title');
}


register_nav_menus( array(
    'topo' => __( 'Menu principal', 'casaninho' ),
) );


//Slider

function create_banner(){

    register_post_type('sliders',
    //opcões
    array(
        'labels' => array(

        'name' => __('Slider Home'),
                ),

        'supports' => array( 'title', 'thumbnail'),
        'public' => true,
        'has_archive' => true,
        'menu_icon' => 'dashicons-format-image',
        'rewrite' => array('slug' => 'sliders'),

        )
    );
    }

    //iniciar banner
    add_action('init', 'create_banner');


//Destaques
function create_equipe(){

    register_post_type('equipe',
    //opcões
    array(
        'labels' => array(

        'name' => __('Equipe e Voluntários'),
        'singular_name' => __('Equipe e Voluntário')
        ),

        'supports' => array( 'title', 'thumbnail', 'editor'),
        'public' => true,
        'menu_icon' => 'dashicons-buddicons-buddypress-logo',
        'rewrite' => array('slug' => 'equipe'),

        )
    );
    }
    
    //iniciar destaques
    add_action('init', 'create_equipe');


    //Destaques
function create_criancas(){

    register_post_type('criancas',
    //opcões
    array(
        'labels' => array(

        'name' => __('Crianças e Adolescentes'),
        'singular_name' => __('Criança e Adolescente')
        ),

        'supports' => array( 'title', 'thumbnail', 'editor'),
        'public' => true,
        'menu_icon' => 'dashicons-universal-access',
        'rewrite' => array('slug' => 'criancas'),

        )
    );
    }
    
    //iniciar destaques
    add_action('init', 'create_criancas');


    //Sidebar
function casaninho_sidebar() {

	register_sidebar(
		array(
			'name'          => 'Sidebar',
			'id'            => 'sidebar-1',
			'before_widget' => '<section id="%1$s" class="widget %2$s">',
			'after_widget'  => '</section>',
			'before_title'  => '<h2 class="widget-title">',
			'after_title'   => '</h2>',
		)
	);

}
add_action( 'widgets_init', 'casaninho_sidebar' );

require get_template_directory() . '/assets/shortcodes.php';



//Slider Estrutura

function create_estrutura_slider(){

    register_post_type('estrutura-slider',
    //opcões
    array(
        'labels' => array(

        'name' => __('Slider Estrutura'),
        ),

        'supports' => array( 'title', 'thumbnail'),
        'public' => true,
        'has_archive' => true,
        'menu_icon' => 'dashicons-format-image',
        'rewrite' => array('slug' => 'estrutura-slider'),

        )
    );
    }

    //iniciar banner
    add_action('init', 'create_estrutura_slider');