<?php
get_header();
?>
<!-- Slider -->
<div class="slider-header mb-5">
    <div class="container p-0">

        <div id="banner" class="carousel-home slide" data-bs-ride="carousel">
        <div class="frame-overlay"></div>

            <div class="carousel-indicators">
              <button class="active carousel-indicator-cn-1" type="button" data-bs-target="#banner" data-bs-slide-to="0" aria-current="true" aria-label="Slide 1"></button>
              <button class="carousel-indicator-cn-2" type="button" data-bs-target="#banner" data-bs-slide-to="1" aria-label="Slide 2"></button>
              <button class="carousel-indicator-cn-3" type="button" data-bs-target="#banner" data-bs-slide-to="2" aria-label="Slide 3"></button>
              <button class="carousel-indicator-cn-4" type="button" data-bs-target="#banner" data-bs-slide-to="3" aria-label="Slide 4"></button>
              <button class="carousel-indicator-cn-5" type="button" data-bs-target="#banner" data-bs-slide-to="4" aria-label="Slide 5"></button>
            </div>

            <div class="carousel-inner">

<?php
$my_args_slider = array(
  'post_type' => 'sliders',
  'post_per_page' => 5
  ,  
);

$my_query_slider = new WP_Query ($my_args_slider);
?>

<?php
if ( $my_query_slider->have_posts()) :
$slider = $sliders[0];
$c = 0;
while ($my_query_slider->have_posts()) :
$my_query_slider->the_post();
?>


              <div class="carousel-item <?php $c++; if ($c == 1) {echo 'active'; } ?>">
                 <?php the_post_thumbnail('post_thumbnail', array( 'class' => 'img-fluid w-100')) ?>
              </div>
            <?php endwhile; endif; ?>
            <?php wp_reset_query();?>

              <button class="carousel-control-prev" type="button" data-bs-target="#banner"  data-bs-slide="prev">
                <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                <span class="visually-hidden">Previous</span>
              </button>
              <button class="carousel-control-next" type="button" data-bs-target="#banner"  data-bs-slide="next">
                <span class="carousel-control-next-icon" aria-hidden="true"></span>
                <span class="visually-hidden">Next</span>
              </button>
        </div>
    </div>
</div>

<!-- Container -->
<div class="container">
  <!-- Conteúdo -->
  <div class="row my-5">
      <?php while(have_posts()) : the_post(); ?>
      <?php the_content(); ?>
      <?php endwhile; ?>
<!-- Final Conteúdo -->
</div>


<!-- Bazar -->
<h1 class="text-center my-5">Bazar</h1>
        <div class="slider-container">
          <div id="bz-feed" class="bz-carousel owl-carousel owl-theme mb-7">
            <?php
            $meta_query  = WC()->query->get_meta_query();
            $tax_query   = WC()->query->get_tax_query();
            $tax_query[] = array(
                'taxonomy' => 'product_visibility',
                'field'    => 'name',
                'terms'    => 'featured',
                'operator' => 'IN',
            );
            
            $args = array(
                'post_type'           => 'product',
                'post_status'         => 'publish',
                'posts_per_page'      => 12,
                'meta_query'          => $meta_query,
                'tax_query'           => $tax_query,
            );
            
            $featured_query = new WP_Query( $args );
                
            if ($featured_query->have_posts()) {
            
                while ($featured_query->have_posts()) : 
                
                    $featured_query->the_post();
                    
                    $product = get_product( $featured_query->post->ID );
                    
            ?>
            <div class="item">
                <a href="<?php the_permalink(); ?>"><?php echo woocommerce_get_product_thumbnail(); ?></a>
            </div>

            <?php endwhile;}?>
          </div> 
          <!-- Final Bazar -->         
        </div>








<!-- Instagram -->
<h1 class="text-center my-5">Instagram</em></h1>
<p class="text-center">Fique pertinho de tudo que acontece em nosso Ninho: Dia-a-dia das crianças e adolescentes acolhidos em nosso ninho e novidades em primeira mão.</p>
        <div class="slider-container">
          <?php echo do_shortcode("[ap_instagram_slider]"); ?>
          <!-- Final Instagram -->
        </div>

<!--<script type="text/javascript">
	var userFeed = new Instafeed({
		get: 'user',
    limit: 12,
    template: '<div class="item"><a href="{{link}}" target="_blank"><img src="{{image}}" class="img-fluid"/></a></div>',
    target: "ig-feed",
    resolution: 'low_resolution',
		accessToken: 'IGQVJVa0lDSkgwRnlNMnp2am5wZATRIaEtsNTBEZA1RJNVBCOWFEaTRyQU9qdnlSWDJqSmR0Q0IwejFSSzJCOFVTNVc4MUZABS0lyZAmRaRXpOOEVpMjhYU3BrSGN2RmlxZAl9zUXhTMnIwVTBoVkc0Wk4zaQZDZD'
	});
	userFeed.run();
	</script> -->



<!-- Blog -->
<h1 class="text-center my-5">Blog</h1>
<p class="text-center">Confira as novidades em nosso #BlogdoNinho, por aqui nós compartilhamos notícias positivas, inspirações, matérias interessantes, novidades e muito mais.</p>
<div class="mt-5">
  <div class="row justify-content-center">
  <?php query_posts('posts_per_page=2'); ?>
  <?php
// Start the Loop.

$the_query = new WP_Query( $args ); 

if ( have_posts() ) : 
    while ( have_posts() ) : the_post();
    
        /* * See if the current post is in category 3.
          * If it is, the div is given the CSS class "post-category-three".
          * Otherwise, the div is given the CSS class "post".
        */
        if ( in_category( 3 ) ) : ?>
        <div class="post-category-three">
        <?php else : ?>
        <div class="col-md-5 col-sm-12 my-2">
    <div class="border-frame-top"></div>
    <div class="row align-items-center p-3"  style="min-height: 200px;">
      <a class="m-2 p-0" style="width: auto; height: auto" href="<?php the_permalink();?>"><?php the_post_thumbnail( array( 'class' => 'img-thumbnail')); ?></a>
        <div class="col-md-7">
          <h5 class="card-title"><em><?php the_title( ); ?></em></h5>
          <p class="card-text"><?php echo wp_trim_words( get_the_content( ), 12, '...' ); 
  ?></p>
        </div>
    </div>
    <div class="border-frame-bottom"></div>

  </div>
      <?php endif; ?>
      <?php 
    // Stop the Loop, but allow for a "if not posts" situation
    endwhile; 
 
else :
    /*
      * The very first "if" tested to see if there were any posts to
      * display. This "else" part tells what do if there weren't any.
     */
     _e( 'Sorry, no posts matched your criteria.', 'textdomain' );
  
// Completely stop the Loop.
 endif;?>
  </div>
      </div>
    </div>
  </div>
</div>

<!-- FinalBlog -->
</div>



<!-- Banner Bottom -->
<div class="container p-0">
  <img src="<?php bloginfo('template_url'); ?>/assets/images/banner-b.jpg" class="w-100">
  <!-- Final Banner Bottom -->
</div>
<!-- Final Container -->
</div>

<?php get_footer(); ?>