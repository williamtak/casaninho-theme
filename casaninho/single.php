<?php
/**
 * The Template for displaying all single posts.
 *
 * @package WordPress
 * @subpackage themename
 */

get_header(); ?>
<div class="container">
<div class="row">
<?php while(have_posts()) : the_post(); ?>

<h1 class="fw-bold my-5"><em><?php the_title(); ?></em></h1>
<h5 class="fw-bold"><em><?php the_time(get_option('date_format')); ?></em></h5>

	<div class="col-md-9">

	<div class="row"><p><?php the_content(); ?></p></div>

<?php endwhile; ?>
</div>

<!--Sidebar-->

<div class="col-md-3">
<?php get_sidebar(); ?>
</div>
</div>

</div>
<?php get_template_part( 'featured-content' ); ?>
<?php get_footer(); ?>