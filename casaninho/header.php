<!DOCTYPE html>
<html <?php language_attributes();?>>
<head>
    <meta charset="<?php bloginfo('charset'); ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <!-- Bootstrap CSS -->
        <link href="<?php bloginfo('template_url'); ?>/main.css" rel="stylesheet">
    
    <!-- Owl CSS -->
    <link rel="stylesheet" href="<?php bloginfo('template_url'); ?>/assets/css/owl.carousel.min.css">
    <link rel="stylesheet" href="<?php bloginfo('template_url'); ?>/assets/css/owl.theme.default.min.css">


    <!-- Google Fonts -->
    <link href="https://fonts.googleapis.com/css2?family=Maven+Pro:wght@400;500;600;700;800;900&family=Roboto:ital,wght@0,100;0,300;0,400;0,500;0,700;0,900;1,100;1,300;1,400;1,500;1,700;1,900&display=swap" rel="stylesheet">

    <?php wp_head(); ?>


</head>

<body>
      <!-- topbar -->
        <div class="bg-cn-green">
            <div class="container">
              <div class="row justify-content-end">
                    <form class="py-3 w-auto" action="/" method="get">
                      <div class="input-group ps-0 col-sm-12">
                            <button class="btn btn-search ms-0" type="submit"></button>
                            <input type="search" name="s" id="search" value="<?php the_search_query(); ?>" class="form-control img-search-bar search-bar me-2 bg-transparent" placeholder="Procurar" style="max-width: 130px;" />
                      </div>
                    </form>
            </div>
          </div>
        </div>

<!-- Menu -->    
        <nav class="navbar navbar-expand-lg navbar-light mt-4">
          <div class="container">
            <div class="col-md-2">

            <?php
              $casaninho_custom_logo = get_theme_mod('custom_logo');
              $logo = wp_get_attachment_image_src($casaninho_custom_logo, 'full');
                 if ( has_custom_logo ()) 
                 {
                   echo '<a href="' . esc_url( home_url( '/' ) ) . '"><img src="' . esc_url($logo[0]) . '" class="img-fluid mx-auto d-block" alt="Casa Ninho" width="109" height="107"></a>';
              }  else {
                echo '<h1>' . get_bloginfo('name') . '</h1>';
            }
            ?>


          </div>

            <div class="nav ms-auto mx-4" id="navbarText">

            <div class="collapse navbar-collapse" id="navbarSupportedContent">
              <ul class="navbar-nav me-auto mb-2 mb-lg-0 align-items-center">
                <li class="nav-item ms-4 active botton-border-menu-1">
                  <a class="nav-link menu-link" aria-current="page" href="<?php echo esc_url( home_url( '/' ) ); ?>">Home</a>
                </li>
                <li class="nav-item ms-4 botton-border-menu-2">
                  <a class="nav-link menu-link" href="sobre">Sobre</a>
                </li>
                <li class="nav-item ms-4 botton-border-menu-3">
                  <a class="nav-link menu-link" href="transparencia">Transparência</a>
                </li>
                <li class="nav-item ms-4 botton-border-menu-4">
                    <a class="nav-link menu-link" href="estrutura">Estrutura</a>
                  </li>
                <li class="nav-item ms-4 botton-border-menu-4">
                    <a class="nav-link menu-link" href="doe">Doe</a>
                  </li>
                  <li class="nav-item ms-4 botton-border-menu-5">
                    <a class="nav-link menu-link" href="bazar">Bazar</a>
                  </li>
                  <li class="nav-item ms-4 botton-border-menu-1">
                    <a class="nav-link menu-link" href="blog">Blog</a>
                  </li>
              </ul>
            </div>
          </div>
          <div class="cart-container">
            <a href="<?php echo esc_url( home_url( 'carrinho' ) ); ?>" id="cart">
 
            <?php global $woocommerce;
    
                    $items = $woocommerce->cart->get_cart();

                    foreach($items as $item => $values) {
                    $quantity = $woocommerce->cart->get_cart_contents_count();
             // accepts 2 arguments ( size, attr )
        } 
?>

                    <img src="<?php bloginfo('template_url'); ?>/assets/images/cart.svg" alt="Carrinho" width="77" height="59" class="menu-cart"></a>
                    <span class="badge"><? echo "$quantity";?></span>

  </div>
              <button id="mobile" class="navbar-toggler mt-4 w-100" type="button">
                        <span class="h2">MENU</span><br>
                        <span class="navbar-toggler-icon h2"></span>
              </button>

    <div class="mb-menu col-12" style="display:none;">
              <div class="row align-items-center p-2" style="height: 80px;">
                <li class="col-2 p-0 text-center active botton-border-menu-1">
                  <a class="menu-mobile-link" aria-current="page" href="<?php echo esc_url( home_url( '/' ) ); ?>">Home</a>
                </li>
                <li class="col-2 p-0 text-center botton-border-menu-2">
                  <a class="menu-mobile-link" href="sobre">Sobre</a>
                </li>
                <li class="col-4 p-0 text-center botton-border-menu-3">
                  <a class="menu-mobile-link" href="transparencia">Transparência</a>
                </li>
                  <li class="col-3 p-0 text-center botton-border-menu-4">
                    <a class="menu-mobile-link" href="estrutura">Estrutura</a>
                  </li>
                  <li class="col-1 p-0 text-center botton-border-menu-4">
                    <a class="menu-mobile-link" href="doe">Doe</a>
                  </li>
                  <li class="col-2 p-0 text-center botton-border-menu-5">
                    <a class="menu-mobile-link" href="bazar">Bazar</a>
                  </li>
                  <li class="col-2 p-0 text-center botton-border-menu-1">
                    <a class="menu-mobile-link" href="blog">Blog</a>
                  </li>
              </ul>
    </div> <!--end shopping-cart -->

</div>
        </nav>

        <!-- Social -->
<div class="container social-media-bar">
      <div class="row justify-content-end">
          <ul class="list-inline w-auto">
              <li class="list-inline-item"><a href="#">
                  <img src="<?php bloginfo('template_url'); ?>/assets/images/instagram.svg" alt="" width="65" height="65" class="social-media-item"></a>              
              </li>
              <li class="list-inline-item"><a href="#">
                  <img src="<?php bloginfo('template_url'); ?>/assets/images/facebook.svg" alt="" width="65" height="65" class="social-media-item"></a>              
              </li>
              <li class="list-inline-item"><a href="#">
                  <img src="<?php bloginfo('template_url'); ?>/assets/images/youtube.svg" alt="" width="65" height="65" class="social-media-item"></a>              
              </li>
          </ul>
      </div>
  </div>


        