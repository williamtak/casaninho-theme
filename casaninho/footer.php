<!-- Footer -->
<div class="container">
    <div class="row justify-content-center">
      <div class="col-md-5 my-2">
        <h1 class="text-center my-5">Redes Sociais</h1>
        <ul class="list-inline text-center">
              <li class="list-inline-item"><a href="https://www.instagram.com/casaninhocaccc/">
                  <img src="<?php bloginfo('template_url'); ?>/assets/images/instagram.svg" alt="" width="65" height="65" class="social-media-item"></a>              
              </li>
              <li class="list-inline-item"><a href="https://www.facebook.com/pg/casaninhocaccc/">
                  <img src="<?php bloginfo('template_url'); ?>/assets/images/facebook.svg" alt="" width="65" height="65" class="social-media-item"></a>              
              </li>
              <li class="list-inline-item"><a href="https://www.youtube.com/channel/UCJAgbFfEbsr_SZSnm23VveQ">
                  <img src="<?php bloginfo('template_url'); ?>/assets/images/youtube.svg" alt="" width="65" height="65" class="social-media-item"></a>              
              </li>
          </ul>
      <h1 class="text-center my-5">Localização</h1>
      <h5 class="text-center">CASA DAS CRIANÇAS</h5>
      <p class="text-center">Rua Almeida Tôrres, 264 – Aclimação<BR>São Paulo / SP, 01530-010 - Fone: (11) 3208-1162<p>
      <h5 class="text-center">CASA DOS ADOLESCENTES</h5>
      <p class="text-center">Avenida Aclimação, 786 - Aclimação<Br>São Paulo / SP, 01534-000 - Fone: (11) 3207-4255 / 3208-8404</p>
                <img class="img-fluid" src="<?php bloginfo('template_url'); ?>/assets/images/mapa.jpg">      
    </div>

    
    <div class="col-md-5 my-2">
      <h1 class="text-center my-5">Contato</h1>

      <div class="row justify-content-center px-5">
      <?php echo do_shortcode('[contact-form-7 id="335"]'); ?>
    
  </div>
  </div>
  
<div class="row p-0">
  <img src="<?php bloginfo('template_url'); ?>/assets/images/bg-footer.jpg" class="img-fluid p-0">
</div>
</div>

</div>

<?php wp_footer(); ?>

    <!-- Scripts -->

    <script src="<?php bloginfo('template_url'); ?>/assets/js/jquery.js"></script>
    <script src="<?php bloginfo('template_url'); ?>/assets/js/owl.carousel.min.js"></script>
    <script src="<?php bloginfo('template_url'); ?>/assets/js/bootstrap.js"></script>
    <script src="<?php bloginfo('template_url'); ?>/assets/js/bootstrap.min.js"></script>
    <script src="<?php bloginfo('template_url'); ?>/assets/js/bootstrap.bundle.min.js"></script>
    <script src="<?php bloginfo('template_url'); ?>/assets/js/popper.js"></script>
    <script src="<?php bloginfo('template_url'); ?>/assets/js/scripts.js"></script>



 <script>
jQuery(function($){
  const nextIcon = '<img src="<?php bloginfo('template_url'); ?>/assets/images/n-arrow.svg" alt:"Próximo">';
const prvIcon = '<img src="<?php bloginfo('template_url'); ?>/assets/images/p-arrow.svg" alt:"Anterior">';

	$('.ig-carousel').owlCarousel({
		loop:true,
		autoPlay:true,
		autoplayHoverPause:true, // if slider is autoplaying, pause on mouse hover
		autoplayTimeout:380,
		autoplaySpeed:800,
		navSpeed:500,
    margin:20,
		dots:true, // dots navigation below the slider
		nav:true, // left and right navigation
    navText:[
      prvIcon,
      nextIcon
    ],
    responsive:{
        0:{
            items:1
        },
        600:{
            items:3
        },
        1000:{
            items:3
        }
    }
	});
	$('.bz-carousel').owlCarousel({
		loop:true,
		autoPlay:true,
		autoplayHoverPause:true, // if slider is autoplaying, pause on mouse hover
		autoplayTimeout:380,
		autoplaySpeed:800,
		navSpeed:500,
    margin:20,
    itemsDesktop : [1000,5], //5 items between 1000px and 901px
      itemsDesktopSmall : [900,3], // betweem 900px and 601px
      itemsTablet: [600,2], //2 items between 600 and 0
      itemsMobile : false, // itemsMobile disabled - inherit from itemsTablet option
		dots:true, // dots navigation below the slider
		nav:true, // left and right navigation
    navText:[
      prvIcon,
      nextIcon
    ],
    responsive:{
        0:{
            items:1
        },
        600:{
            items:3
        },
        1000:{
            items:4
        }
    }
	});
});


</script>


  </body>
</html>