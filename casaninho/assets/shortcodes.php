<?php
function criancas_shortcode() {
	ob_start();
	?> <HTML><!-- Crianças -->
            <div class="slider-container my-5">
            <div id="bz-feed" class="bz-carousel owl-carousel owl-theme mb-7">
    
            <?php
                        $meta_query  = WC()->query->get_meta_query();
                        $tax_query   = WC()->query->get_tax_query();
                        $tax_query[] = array(
                            'field'    => 'name',
                            'operator' => 'IN',
                        );
                $args = array(
                    'post_type'           => 'criancas',
                    'post_status'         => 'publish',
                    'posts_per_page'      => 12,
                );
                
                $criancas_query = new WP_Query( $args );
                    
                if ($criancas_query->have_posts()) {
                
                    while ($criancas_query->have_posts()) : 
                    
                        $criancas_query->the_post();
                        
                        
                ?>
                  <div class="item">
                  <?php the_post_thumbnail('post_thumbnail', array( 'class' => 'img-thumbnail w-100')) ?>
                  </div>
                       <?php endwhile;}?>
              </div>
              <!-- Final Crianças -->
              </div>
    <?php
	return ob_get_clean();
}

add_shortcode('slider_criancas', 'criancas_shortcode');





function equipe_shortcode() {
	ob_start();
	?> <HTML>
        <!-- Equipe -->
<div class="slider-container my-5">
<div id="bz-feed" class="bz-carousel owl-carousel owl-theme mb-7">

<?php
            $meta_query  = WC()->query->get_meta_query();
            $tax_query   = WC()->query->get_tax_query();
            $tax_query[] = array(
                'field'    => 'name',
                'operator' => 'IN',
            );
    $args = array(
        'post_type'           => 'equipe',
        'post_status'         => 'publish',
        'posts_per_page'      => 32,
    );
    
    $equipe_query = new WP_Query( $args );
        
    if ($equipe_query->have_posts()) {
    
        while ($equipe_query->have_posts()) : 
        
            $equipe_query->the_post();
            
            
    ?>
      <div class="item">
      <?php the_post_thumbnail('post_thumbnail', array( 'class' => 'img-thumbnail w-100')) ?>
      </div>
      
      <?php endwhile;}?>
      
  </div>
  <!-- Final Equipe -->
  </div>

<?php
	return ob_get_clean();
}

add_shortcode('slider_equipe', 'equipe_shortcode');





function estrutura_slider_shortcode() {
	ob_start();
	?> <HTML><!-- Slider -->
        <div class="container p-0">
    
            <div id="banner" class="carousel slide" data-bs-ride="carousel">
    
                <div class="carousel-indicators">
                  <button class="active carousel-indicator-cn-1" type="button" data-bs-target="#banner" data-bs-slide-to="0" aria-current="true" aria-label="Slide 1"></button>
                  <button class="carousel-indicator-cn-2" type="button" data-bs-target="#banner" data-bs-slide-to="1" aria-label="Slide 2"></button>
                  <button class="carousel-indicator-cn-3" type="button" data-bs-target="#banner" data-bs-slide-to="2" aria-label="Slide 3"></button>
                  <button class="carousel-indicator-cn-4" type="button" data-bs-target="#banner" data-bs-slide-to="3" aria-label="Slide 4"></button>
                  <button class="carousel-indicator-cn-5" type="button" data-bs-target="#banner" data-bs-slide-to="4" aria-label="Slide 5"></button>
                </div>
    
                <div class="carousel-inner">
    
    <?php
    $my_args_slider = array(
      'post_type' => 'estrutura-slider',
      'post_per_page' => 5
      ,  
    );
    
    $my_query_slider = new WP_Query ($my_args_slider);
    ?>
    
    <?php
    if ( $my_query_slider->have_posts()) :
    $slider = $sliders[0];
    $c = 0;
    while ($my_query_slider->have_posts()) :
    $my_query_slider->the_post();
    ?>
    
    
                  <div class="carousel-item <?php $c++; if ($c == 1) {echo 'active'; } ?>">
                     <?php the_post_thumbnail('post_thumbnail', array( 'class' => 'img-fluid w-100')) ?>
                  </div>
                <?php endwhile; endif; ?>
                <?php wp_reset_query();?>
    
                  <button class="carousel-control-prev" type="button" data-bs-target="#banner"  data-bs-slide="prev">
                    <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                    <span class="visually-hidden">Previous</span>
                  </button>
                  <button class="carousel-control-next" type="button" data-bs-target="#banner"  data-bs-slide="next">
                    <span class="carousel-control-next-icon" aria-hidden="true"></span>
                    <span class="visually-hidden">Next</span>
                  </button>
            </div>
        </div>
    </div>

<?php
	return ob_get_clean();
}

add_shortcode('slider_estrutura', 'estrutura_slider_shortcode');

?>

