<script>
jQuery(function($){
  var owl = $(".owl-carousel"),
        owlSlideSpeed = 300;

    // init owl    
    $(document).ready(function(){

  const nextIcon = '<img src="<?php bloginfo('template_url'); ?>/assets/images/n-arrow.svg" alt:"Próximo">';
const prvIcon = '<img src="<?php bloginfo('template_url'); ?>/assets/images/p-arrow.svg" alt:"Anterior">';

	$('.ig-carousel').owlCarousel({
		loop:true,
		autoPlay:true,
		autoplayHoverPause:true, // if slider is autoplaying, pause on mouse hover
		autoplayTimeout:380,
		autoplaySpeed:800,
		navSpeed:500,
    margin:20,
		dots:true, // dots navigation below the slider
		nav:true, // left and right navigation
    navText:[
      prvIcon,
      nextIcon
    ],
    responsive:{
        0:{
            items:1
        },
        600:{
            items:3
        },
        1000:{
            items:3
        }
    }
	});
	$('.bz-carousel').owlCarousel({
		loop:true,
		autoPlay:true,
		autoplayHoverPause:true, // if slider is autoplaying, pause on mouse hover
		autoplayTimeout:380,
		autoplaySpeed:800,
		navSpeed:500,
    margin:20,
		dots:true, // dots navigation below the slider
		nav:true, // left and right navigation
    navText:[
      prvIcon,
      nextIcon
    ],
    responsive:{
        0:{
            items:1
        },
        600:{
            items:3
        },
        1000:{
            items:4
        }
    }
	});
});
	});