<?php get_header(); ?>

<div class="container pages">
	<div class="row">
			<?php while(have_posts()) : the_post(); ?>
				<p><?php the_content(); ?></p>
			<?php endwhile; ?>
		<img src="<?php bloginfo('template_url'); ?>/assets/images/bg-pages.jpg">
		<!-- Final Row -->
	</div>
<!-- Final Container -->
</div>

<?php get_footer(); ?>