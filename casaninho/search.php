<?php
/**
 * The Template for displaying all single posts.
 *
 * @package WordPress
 * @subpackage themename
 */

get_header(); ?>
<div class="container">
<div class="row">
<div class="col-9">
<?php while(have_posts()) : the_post(); ?>

	<div class="row"><h1><?php the_title(); ?></h1></div>

	<div class="row"><p> <?php the_time(get_option('date_format')); ?></p></div>

	<div class="row"><p><?php the_content(); ?></p></div>

<?php endwhile; ?>
</div>
<div class="col-3">

<?php get_sidebar(); ?>

</div>
</div>

</div>
<?php get_sidebar(); ?>
<?php get_template_part( 'featured-content' ); ?>
<?php get_footer(); ?>