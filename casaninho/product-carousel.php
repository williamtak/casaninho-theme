<?php
get_header();
?><!-- Container -->
<div class="container">
  <!-- Conteúdo -->
  <div class="row my-5">
      <?php while(have_posts()) : the_post(); ?>
      <?php the_content(); ?>
      <?php endwhile; ?>
<!-- Final Conteúdo -->
</div>



<?php


public function get_featured( $context = 'view' ) {
  return $this->get_prop( 'featured', $context );

}?>

<?php get_footer(); ?>